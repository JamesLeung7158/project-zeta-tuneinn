from django.db import models
from django.urls import reverse

class UserVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, null=True)
    username = models.CharField(max_length=200, unique=True)
    horoscope = models.CharField(max_length=100, null=True)
    def __str__(self):
        return self.username
    
class Playlist(models.Model):
    name = models.CharField(max_length=100)
    
    def __str__(self):
        return self.name
    def get_api_url(self):
        return reverse("show_playlist_details", kwargs={"id": self.id})

class Song(models.Model):
    title = models.CharField(max_length=100)
    artist = models.CharField(max_length=100)
    genre = models.CharField(max_length=100)
    song_url = models.URLField(null=False)
    playlist = models.ForeignKey(
        Playlist,
        related_name="songs",
        on_delete=models.PROTECT
    )

    def __str__(self):
        return self.title
    def get_api_url(self):
        return reverse("show_song_details", kwargs={"id": self.id})


class Card(models.Model):
    card_title = models.CharField(max_length=100)
    date_created = models.DateTimeField(auto_now_add=True)
    card_picture = models.URLField(null=True)
    playlist = models.ForeignKey(
        Playlist, 
        related_name="playlist",
        on_delete=models.PROTECT
    )
    username = models.ForeignKey(
        UserVO,
        related_name="user",
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.card_title
    def get_api_url(self):
        return reverse("show_card_details", kwargs={"id": self.id})
    