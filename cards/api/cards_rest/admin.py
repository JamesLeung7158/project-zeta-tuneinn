from django.contrib import admin
from .models import Card, Playlist, Song, UserVO

admin.site.register(Card)
admin.site.register(Playlist)
admin.site.register(Song)
admin.site.register(UserVO)
