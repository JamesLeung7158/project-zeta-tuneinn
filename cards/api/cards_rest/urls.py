from django.urls import path 
from .views import song_list, show_song_details, playlist_list, show_playlist_details, card_list, show_card_details

urlpatterns = [
    path("songs/", song_list, name="song_list"),
    path("songs/<int:id>/", show_song_details, name="show_song_details"),
    path("playlist/", playlist_list, name="playlist_list"),
    path("playlist/<int:id>/", show_playlist_details, name="show_playlist_details"),
    path("card_list/", card_list, name="card_list"),
    path("card_list/<int:id>/", show_card_details, name="show_card_details"),
]