from django.apps import AppConfig


class CardsRestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cards_rest'
