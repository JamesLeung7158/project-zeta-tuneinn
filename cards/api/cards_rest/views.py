from django.shortcuts import render
from django.views.decorators.http import require_http_methods
# from django.contrib.auth.decorators import login_required
from .models import Card, Song, Playlist, UserVO
from django.http import JsonResponse
from common.json import ModelEncoder
import json

class UserEncoder(ModelEncoder):
    model = UserVO
    properties = [
        "username",
        "horoscope",
        "import_href",
    ]


class PlaylistEncoder(ModelEncoder):
    model = Playlist
    properties = [
        "name",
        "id",
    ]


class SongListEncoder(ModelEncoder):
    model = Song
    properties = [
        "title",
        "genre",
        "artist",
        "song_url",
        "playlist",
        "id",
    ]

    encoders = {
        "playlist": PlaylistEncoder(),
    }


class CardListEncoder(ModelEncoder):
    model = Card
    properties = [
        "card_title",
        "date_created",
        "card_picture",
        "playlist",
        "username",
        "id",
    ]
    encoders = {
        "playlist": PlaylistEncoder(),
        "username": UserEncoder(),
    }
#
#Song views
#
@require_http_methods(["GET", "POST"])
def song_list(request):
    if request.method == "GET":
        songs = Song.objects.all()
        return JsonResponse(
            {"songs": songs},
            encoder=SongListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            playlist_id = content["playlist"]
            playlist = Playlist.objects.get(id=playlist_id)
            content["playlist"] = playlist

            song = Song.objects.create(**content)
            return JsonResponse(
                song,
                encoder=SongListEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Invalid Song Information"},
                status=400,
            )

@require_http_methods(["GET", "PUT", "DELETE"])
def show_song_details(request, id):
    if request.method == "GET":
        song = Song.objects.get(id=id)
        return JsonResponse(
            song,
            encoder=SongListEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            song = Song.objects.get(id=id)
            song.delete()
            return JsonResponse(
                {"Song Deleted": True}
            )
        except Song.DoesNotExist:
            return JsonResponse(
                {"message": "This Song Doesn't Exist"},
                status=400
            )
    else:
        content = json.loads(request.body)
        Song.objects.filter(id=id).update(**content)
        song = Song.objects.get(id=id)
        return JsonResponse(
            song,
            encoder=SongListEncoder,
            safe=False
        )
#
# Playlist stuff
#
@require_http_methods(["GET", "POST"])
def playlist_list(request):
    if request.method == "GET":
        playlist = Playlist.objects.all()
        return JsonResponse(
            {"playlist": playlist},
            encoder=PlaylistEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        # try:
        playlist = Playlist.objects.create(**content)
        #     card = Card.objects.get(id=card_id)
        #     content["card"] = card
        # except Card.DoesNotExist:
        #     return JsonResponse(
        #         {"message": "Invalid Card ID"},
        #         status=400,
        #     )

        return JsonResponse(
            playlist,
            encoder=PlaylistEncoder,
            safe=False,
        )
        # except:
        #     return JsonResponse(
        #         {"message": "Invalid Playlist Info"},
        #         status=400,
        #     )

@require_http_methods(["GET", "PUT", "DELETE"])
def show_playlist_details(request, id):
    if request.method == "GET":
        playlist = Playlist.objects.get(id=id)
        return JsonResponse(
            playlist,
            encoder=PlaylistEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            playlist = Playlist.objects.get(id=id)
            playlist.delete()
            return JsonResponse(
                {"Playlist Deleted": True}
            )
        except Playlist.DoesNotExist:
            return JsonResponse(
                {"message": "This Playlist Exist"},
                status=400
            )
    else:
        content = json.loads(request.body)
        Playlist.objects.filter(id=id).update(**content)
        playlist = Playlist.objects.get(id=id)
        return JsonResponse(
            playlist,
            encoder=PlaylistEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def card_list(request):
    if request.method == "GET":
        cards = Card.objects.all()
        return JsonResponse(
            {"cards": cards},
            encoder=CardListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            username = content["username"]
            user = UserVO.objects.get(username=username)
            content["username"] = user
        except:
            return JsonResponse(
                {"message": "Invalid User Information"},
                status=400
            )
        try:
            playlist_id = content["playlist"]
            playlist = Playlist.objects.get(id=playlist_id)
            print("--------->>>>", playlist)
            content["playlist"] = playlist

        except:
            return JsonResponse(
                {"message": "Invalid Playlist Information"},
                status=400
            )
        print("-------->>>>>>", content)
        card = Card.objects.create(**content)
        print("-------", card)
        return JsonResponse(
            card,
            encoder=CardListEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def show_card_details(request, id):
    if request.method == "GET":
        card = Card.objects.get(id=id)
        return JsonResponse(
            card,
            encoder=CardListEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            card = Card.objects.get(id=id)
            card.delete()
            return JsonResponse(
                {"Card Deleted": True}
            )
        except Card.DoesNotExist:
            return JsonResponse(
                {"message": "This Card Doesn't Exist"},
                status=400,
            )
    else:
        content = json.loads(request.body)
        Card.objects.filter(id=id).update(**content)
        card = Card.objects.get(id=id)
        return JsonResponse(
            card,
            encoder=CardListEncoder,
            safe=False,
        )
