import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cards_project.settings")
django.setup()

from cards_rest.models import UserVO;

def get_user():
    response = requests.get("http://accounts-api:8000/api/users/")
    content = json.loads(response.content)
    for user in content["users"]:
        UserVO.objects.update_or_create(
            import_href = user["href"],
            defaults = {
                "username": user["username"],
                "horoscope": user["horoscope"],
            }
        )
        print("Card poller is working!")

def poll():
    while True:
        print("Card poller polling for data")
        try:
            get_user()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)

if __name__ == "__main__":
    poll()
