import React, { useEffect, useState } from 'react';
import CreatePlaylist from './CreatePlaylist';
import PlaylistCard from './PlaylistCard';

const Playlists = () => {
    const [playlists, setPlaylists] = useState([]);

    const fetchPlaylistData = async () => {
        const playlistUrl = 'http://localhost:8090/cards/playlist/';
        const response = await fetch(playlistUrl);
        if (response.ok) {
            const data = await response.json();
            setPlaylists(data.playlist);
        }
    }

    useEffect(() => {
        fetchPlaylistData();
    }, []);

    return (
        <>
        <br />
        <h1 className="text-center"><strong>Playlists</strong></h1>
        <br />
        <div className="container mt-4">
            <div className="row justifiy-content-end text-center">
                <button type="button" className="btn btn-primary btn-lg btn-block" data-bs-toggle="modal" data-bs-target="#createplaylist" data-cs-whatever="@mdo">Create a new Playlist</button>
            </div>
        </div>
        <CreatePlaylist fetchPlaylistData={fetchPlaylistData} playlists={playlists} />

        <div className="container mt-4">
        <div className="row gy-3">
            <PlaylistCard playlists={playlists} />
        </div>
        </div>
        </>
    );
}

export default Playlists;
