const PlaylistCard = ({ playlists }) => {
    return (
        <div className="row">
            {playlists?.map((playlist) => {
                return (
                <div className="col-md-4 mb-4" key={playlist.id}>
                    <div className="card shadow h-100">
                    <div className="card-body">
                        <h5 className="card-title fs-6"><strong>Playlist Name:</strong></h5>
                        <p className="card-text fs-4">{playlist.name}</p>
                    </div>
                    </div>
                </div>
                );
            })}
        </div>
    );
}

export default PlaylistCard;
