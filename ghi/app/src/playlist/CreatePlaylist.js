import { useEffect, useState } from 'react';

const CreatePlaylist = ({ fetchPlaylistData }) => {
    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        setName(event.target.value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;

        const playlistUrl = 'http://localhost:8090/cards/playlist/';
        const fetchConfigUrl = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch (playlistUrl, fetchConfigUrl);

        if (response.ok) {
            setName('');
        }
        window.location.reload();
    }

    useEffect(() => {
        fetchPlaylistData();
    }, []);

    return (
        <div className="modal fade" id="createplaylist" tabIndex="-1" aria-labelledby="createplaylistLabel" aria-hidden="true">
            <div className="modal-dialog">
            <div className="modal-content">
                <div className="modal-header">
                <h5 className="modal-title" id="createplaylistLabel"><strong>Create a new Playlist</strong></h5>
                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div className="modal-body">
                <form onSubmit={handleSubmit}>
                    <div className="mb-3">
                    <label htmlFor="name" className="col-form-label">Name</label>
                    <input value={name} onChange={handleNameChange} type="text" className="form-control" id="name" />
                    </div>

                    <div className="modal-footer">
                    <button data-bs-dismiss="modal" type="submit" className="btn btn-primary">Create</button>
                    </div>
                </form>
                </div>
            </div>
            </div>
        </div>
    );
}

export default CreatePlaylist;
