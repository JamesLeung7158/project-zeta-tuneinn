import React, { useEffect, useState } from 'react';
import UserCard from './UserCard';
import CreateUser from './CreateUser';


const Users = () => {
    const [users, setUsers] = useState([]);

    async function fetchUsersData() {
        const usersUrl = 'http://localhost:8100/api/users/'
        const response = await fetch(usersUrl);
        if (response.ok) {
            const data = await response.json();
            setUsers(data.users);
        }
    }

    useEffect(() => {
        fetchUsersData()
    }, []);

    return (
        <>
        <br />
        <h1 className="text-center"><strong>Users</strong></h1>
        <br />
        <div className="container mt-4">
            <div className="row justifiy-content-end text-center">
                <button type="button" className="btn btn-primary btn-lg btn-block" data-bs-toggle="modal" data-bs-target="#createuser" data-cs-whatever="@mdo">Create a new User</button>
            </div>
        </div>
        <CreateUser fetchUsersData={fetchUsersData} users={users} />

        <div className="container mt-4">
        <div className="row gy-3">
            <UserCard users={users} />
        </div>
        </div>
        </>
    );
}

export default Users;
