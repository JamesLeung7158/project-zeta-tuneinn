import { useState } from "react";

const CreateUser = () => {
    const [username, setUsername] = useState('');
    const handleUsernameChange = (event) => {
        setUsername(event.target.value);
    }
    const [firstName, setFirstName] = useState('');
    const handleFirstNameChange = (event) => {
        setFirstName(event.target.value);
    }
    const [lastName, setLastName] = useState('');
    const handleLastNameChange = (event) => {
        setLastName(event.target.value);
    }
    const [email, setEmail] = useState('');
    const handleEmailChange = (event) => {
        setEmail(event.target.value);
    }
    const [horoscope, setHoroscope] = useState('');
    const handleHoroscopeChange = (event) => {
        setHoroscope(event.target.value);
    }
    const [password, setPassword] = useState('');
    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.username = username;
        data.first_name = firstName;
        data.last_name = lastName;
        data.email = email;
        data.horoscope = horoscope;
        data.password = password

        const userUrl = 'http://localhost:8100/api/users/'
        const fetchConfigUrl = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(userUrl, fetchConfigUrl);

        if (response.ok) {
            setUsername('')
            setFirstName('')
            setLastName('')
            setEmail('')
            setHoroscope('')
            setPassword('')
        }
        window.location.reload();
    }

    return (
        <div className="modal fade" id="createuser" tabIndex="-1" aria-labelledby="createuserLabel" aria-hidden="true">
            <div className="modal-dialog">
            <div className="modal-content">
                <div className="modal-header">
                <h5 className="modal-title" id="createuserLabel"><strong>Create a new User</strong></h5>
                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div className="modal-body">
                <form onSubmit={handleSubmit}>
                    <div className="mb-3">
                    <label htmlFor="username" className="col-form-label">Username</label>
                    <input value={username} onChange={handleUsernameChange} type="text" className="form-control" id="username" />
                    </div>
                    <div className="mb-3">
                    <label htmlFor="firstname" className="col-form-label">First Name</label>
                    <input value={firstName} onChange={handleFirstNameChange} type="text" className="form-control" id="firstname" />
                    </div>
                    <div className="mb-3">
                    <label htmlFor="lastname" className="col-form-label">Last Name</label>
                    <input value={lastName} onChange={handleLastNameChange} type="text" className="form-control" id="lastname" />
                    </div>
                    <div className="mb-3">
                    <label htmlFor="email" className="col-form-label">Email</label>
                    <input value={email} onChange={handleEmailChange} type="text" className="form-control" id="email" />
                    </div>
                    <div className="mb-3">
                    <label htmlFor="horoscope" className="col-form-label">Horoscope</label>
                    <input value={horoscope} onChange={handleHoroscopeChange} className="form-control" id="horoscope" />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="password" className="col-form-label">Password</label>
                        <input value={password} onChange={handlePasswordChange} className="form-control" id="passcode" />
                    </div>
                    <div className="modal-footer">
                    <button data-bs-dismiss="modal" type="submit" className="btn btn-primary">Create</button>
                    </div>
                </form>
                </div>
            </div>
            </div>
        </div>
    );
}

export default CreateUser;
