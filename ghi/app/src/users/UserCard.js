const UserCard = ({ users }) => {
    return (
        <div className="row">
            {users?.map((user) => {
                return (
                <div className="col-md-6 mb-4" key={user.id}>
                    <div className="card shadow h-100">
                    <div className="card-header">
                        <h5 className="card-title fs-6"><strong>Username:</strong></h5>
                        <p className="card-text fs-4">{user.username}</p>
                    </div>
                    <div className="card-body">
                        <h5 className="card-title fs-6"><strong>First/Last Name:</strong></h5>
                        <p className="card-text fs-4">{user.first_name} {user.last_name}</p>
                        <h5 className="card-title fs-6"><strong>Email:</strong></h5>
                        <p className="card-text fs-4">{user.email}</p>
                    </div>
                    <div className="card-footer">
                        <h5 className="card-title fs-6"><strong>Horoscope:</strong></h5>
                        <p className="card-text fs-4">{user.horoscope}</p>
                    </div>
                    </div>
                </div>
                );
            })}
        </div>
    );
}

export default UserCard;
