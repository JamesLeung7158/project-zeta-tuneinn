import { NavLink } from 'react-router-dom';
import { useState } from 'react';
import './Nav.css';

function Nav() {
    const [loggedIn, setLoggedIn] = useState(false);

    const handleLogin = () => {
        setLoggedIn(true);
    }

    const handleLogout = () => {
        setLoggedIn(false);
    }

    return (
        <nav className="navbar navbar-expand-lg navbar-dark" style={{ backgroundColor: '#00a698', padding: '1rem' }}>
            <div className="container-fluid">
                <NavLink className="navbar-brand" to="/" style={{ fontSize: '2rem', fontWeight: 'bold', color: '#fff' }}>Project Zeta</NavLink>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                {loggedIn ? (
                    <>
                        <li className="nav-item">
                        <NavLink className="nav-link active" to="cards/" style={{ fontSize: '1.2rem', color: '#fff', marginRight: '1rem' }}>Music Profiles</NavLink>
                        </li>
                        <li className="nav-item">
                        <NavLink className="nav-link active" to="playlists/" style={{ fontSize: '1.2rem', color: '#fff', marginRight: '1rem' }}>Playlists</NavLink>
                        </li>
                        <li className="nav-item">
                        <NavLink className="nav-link active" to="songs/" style={{ fontSize: '1.2rem', color: '#fff', marginRight: '1rem' }}>Song List</NavLink>
                        </li>
                    </>
                ) : (
                    <>
                        <li className="nav-item">
                        <NavLink className="nav-link active" to="users/" style={{ fontSize: '1.2rem', color: '#fff', marginRight: '1rem' }}>Users</NavLink>
                        </li>
                        <li className="nav-item">
                        <button className="btn btn-light" onClick={handleLogin}>Login</button>
                        </li>
                    </>
                    )}
                </ul>
                {loggedIn && (
                    <button className="btn btn-light" onClick={handleLogout}>Logout</button>
                )}
                </div>
            </div>
        </nav>
    );
}

export default Nav;
