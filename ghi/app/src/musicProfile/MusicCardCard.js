const MusicCardCard = ({ cards }) => {
    return (
        <div className="row">
            {cards?.map((card) => {
                return (
                <div className="col-md-4 mb-4" key={card.id}>
                    <div className="card shadow h-100">
                    <div className="card-header">
                        <h5 className="card-title fs-6"><strong>Name:</strong></h5>
                        <p className="card-text fs-4">{card.card_title}</p>
                        <h5 className="card-title fs-6"><strong>Username:</strong></h5>
                        <p className="card-text fs-4">{card.username.username}</p>
                        {/* <h5 className="card-title fs-6"><strong>Date Created:</strong></h5>
                        <p className="card-text fs-4">{card.date_created}</p> */}
                    </div>
                    <div className="card-body">
                        <h5 className="card-title fs-6"><strong>Card Picture:</strong></h5>
                        <img src={card.card_picture} />
                    </div>
                    <div className="card-footer">
                        <h5 className="card-title fs-6"><strong>Playlist:</strong></h5>
                        <p className="card-text fs-4">{card.playlist.name}</p>
                    </div>
                    </div>
                </div>
                );
            })}
        </div>
    );
}

export default MusicCardCard;
