import { useEffect, useState } from 'react';

const CreateMusicCard = () => {
    const [username, setUsername] = useState('');
    const handleUsernameChange = (event) => {
        setUsername(event.target.value);
    }
    const [cardtitle, setCardtitle] = useState('');
    const handleCardtitleChange = (event) => {
        setCardtitle(event.target.value);
    }
    const [date, setDate] = useState('');
    const handleDateChange = (event) => {
        setDate(event.target.value);
    }
    const [picture, setPicture] = useState('');
    const handlePictureChange = (event) => {
        setPicture(event.target.value);
    }
    const [playlists, setPlaylists] = useState([]);
    const [playlist, setPlaylist] = useState('');
    const handlePlaylistChange = (event) => {
        setPlaylist(event.target.value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.username.username = username;
        data.card_title = cardtitle;
        data.date_changed = date;
        data.card_picture = picture;

        const musicCardUrl = 'http://localhost:8090/cards/card_list/';
        const fetchConfigUrl = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(musicCardUrl, fetchConfigUrl);

        if (response.ok) {
            setUsername('')
            setDate('')
            setPicture('')
        }
        window.location.reload();
    }

    const fetchPlaylistData = async () => {
        const playlistUrl = 'http://localhost:8090/cards/playlist/';
        const response = await fetch(playlistUrl);
        if (response.ok) {
            const data = await response.json();
            setPlaylists(data.playlist);
        }
    }

    useEffect(() => {
        fetchPlaylistData();
    }, []);

    return (
        <div className="modal fade" id="createmusiccard" tabIndex="-1" aria-labelledby="createmusiccardLabel" aria-hidden="true">
            <div className="modal-dialog">
            <div className="modal-content">
                <div className="modal-header">
                <h5 className="modal-title" id="createmusiccardLabel"><strong>Create a new Music Card</strong></h5>
                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div className="modal-body">
                <form onSubmit={handleSubmit}>
                    <div className="mb-3">
                    <label htmlFor="username" className="col-form-label">Username</label>
                    <input value={username} onChange={handleUsernameChange} type="text" className="form-control" id="username" />
                    </div>
                    <div className="mb-3">
                    <label htmlFor="cardtitle" className="col-form-label">Card Title</label>
                    <input value={cardtitle} onChange={handleCardtitleChange} type="text" className="form-control" id="cardtitle" />
                    </div>
                    <div className="mb-3">
                    <label htmlFor="date" className="col-form-label">Date</label>
                    <input value={date} onChange={handleDateChange} type="date" className="form-control" id="date" />
                    </div>
                    <div className="mb-3">
                    <label htmlFor="pictureUrl" className="col-form-label">Picture Url</label>
                    <input value={picture} onChange={handlePictureChange} className="form-control" id="pictureUrl" />
                    </div>

                    <div className="mb-3">
                    <label htmlFor="playlist" className="col-form-label">Playlist</label>
                    <select value={playlist} onChange={handlePlaylistChange} className="form-select" aria-label="Default select example">
                        <option value="">Select a Playlist</option>
                        {playlists?.map(playlist => {
                        return (
                            <option value={playlist.name} key={playlist.id}>{playlist.name}</option>
                        )
                        })}
                    </select>
                    </div>

                    <div className="modal-footer">
                    <button data-bs-dismiss="modal" type="submit" className="btn btn-primary">Create</button>
                    </div>
                </form>
                </div>
            </div>
            </div>
        </div>
    );
}

export default CreateMusicCard;
