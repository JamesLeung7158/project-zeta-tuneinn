import React, { useEffect, useState } from 'react';
import CreateMusicCard from './CreateMusicCard';
import MusicCardCard from './MusicCardCard';

const MusicCards = () => {
    const [musicCards, setMusicCards] = useState([]);

    async function fetchMusicCardData() {
        const musicCardsUrl = 'http://localhost:8090/cards/card_list/';
        const response = await fetch(musicCardsUrl);
        if (response.ok) {
            const data = await response.json();
            setMusicCards(data.cards);
        }
    }

    useEffect(() => {
        fetchMusicCardData()
    }, []);

    return (
        <>
        <br />
        <h1 className="text-center"><strong>Music Profile Cards</strong></h1>
        <br />
        <div className="container mt-4">
            <div className="row justifiy-content-end text-center">
                <button type="button" className="btn btn-primary btn-lg btn-block" data-bs-toggle="modal" data-bs-target="#createmusiccard" data-cs-whatever="@mdo">Create a new Music Card</button>
            </div>
        </div>
        <CreateMusicCard fetchMusicCardData={fetchMusicCardData} musicCards={musicCards} />

        <div className="container mt-4">
        <div className="row gy-3">
            <MusicCardCard cards={musicCards} />
        </div>
        </div>
        </>
    );
}

export default MusicCards;
