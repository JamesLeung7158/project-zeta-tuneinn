import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

const Login = () => {
    const [username, setUsername] = useState('');
    const handleUsernameChange = (event) => {
        setUsername(event.target.value);
    }
    const [password, setPassword] = useState('');
    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
    }

    const navigate = useNavigate();

    const handleSubmit = async (event) => {
        event.preventDefault();

        // const data = {};
        // data.username = username;
        // data.password = password;

        // if (data.accounts.username === username && data.accounts.password === password)
        try {
            const loginUrl = 'http://localhost:8100/api/users/'
            const fetchConfigUrl = {
                method: "post",
                body: JSON.stringify({ username, password }),
                headers: {
                    'Content-Type': 'application/json',
                },
            }
            const response = await fetch(loginUrl, fetchConfigUrl);

            if (response.ok) {
                navigate('/');
            } else {
                alert('Wrong username or password');
            }
        } catch (error) {
            console.error('An error occured while logging in', error);
        }
    };

    return (
        <div className="container mt-5">
            <div className="row justify-content-center">
                <div className="col-md-6">
                    <div className="card">
                        <div className="card-header">
                        <h1>Login</h1>
                        </div>
                        <div className="card-body">
                        <form onSubmit={handleSubmit}>
                            <div className="form-group">
                                <label htmlFor="username">Username:</label>
                                <input type="text" className="form-control" id="username" name="username" value={username} onChange={handleUsernameChange} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="password">Password:</label>
                                <input type="password" className="form-control" id="password" name="password" value={password} onChange={handlePasswordChange} />
                            </div>
                            <button type="submit" className="btn btn-primary">Login</button>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Login;
