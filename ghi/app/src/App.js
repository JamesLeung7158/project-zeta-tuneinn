import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import MusicCards from './musicProfile/MusicCardPage';
import SongList from './song/SongPage';
import Login from './login/LoginPage';
import Users from './users/UserPage';
import Playlists from './playlist/PlaylistPage';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="cards/" element={<MusicCards />} />
          <Route path="songs/" element={<SongList />} />
          <Route path="login/" element={<Login />} />
          <Route path="users/" element={<Users />} />
          <Route path="playlists/" element={<Playlists />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
