import './MainPage.css';
import MusicCardCard from './musicProfile/MusicCardCard';

function MainPage({ musicCards, fetchMusicCardData }) {
    return (
        <div className="main-page">
            <i className="bi bi-music-note fs-1 mb-3"></i>
            <h1 className="display-5 fw-bold text-center">TuneInn</h1>
            <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">
                A music hub to find your next favorite song!
            </p>
            </div>
            <div className="container my-5">
                <h2 className="fw-bold mb-3 text-center"><strong>Featured Playlists</strong></h2>
                <MusicCardCard cards={musicCards} fetchMusicCardData={fetchMusicCardData} />
            </div>
        </div>
    );
}

export default MainPage;
