import React, { useState } from 'react';

function CreateSong() {

    const [song_title, setTitle] = useState('');
    const [artist, setArtist] = useState('');

    const handleSongTitle = (event) => {
        const value = event.target.value;
        setTitle(value)
    }

    const handleArtist = (event) => {
        const value = event.target.value;
        setArtist(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.title = song_title;
        data.artist = artist;

        // Need that new song form url cuz pls
        const newSongFormUrl = "http://localhost:8090/cards/songs/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(newSongFormUrl, fetchConfig);
        if (response.ok) {
            setTitle('');
            setArtist('');
        }
    }

    return (
        <div className="modal fade" id="createsong" tabIndex="-1" aria-labelledby="createsongLabel" aria-hidden="true">
            <div className="modal-dialog">
            <div className="modal-content">
                <div className="modal-header">
                <h5 className="modal-title" id="createsongLabel"><strong>Create new song</strong></h5>
                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="close"></button>
                </div>
                <div className="modal-body">
                <form onSubmit={handleSubmit}>
                    <div className="mb-3">
                    <label htmlFor="song_title" className="col-form-label">Title</label>
                    <input value={song_title} onChange={handleSongTitle} type="text" className="form-control" id="song_title" />
                    </div>
                    <div className="mb-3">
                    <label htmlFor="artist" className="col-form-label">Artist</label>
                    <input value={artist} onChange={handleArtist} type="text" className="form-control" id="artist" />
                    </div>
                    <div className="modal-footer">
                    <button data-bs-dismiss="modal" type="submit" className="btn btn-primary">Create</button>
                    </div>
                </form>
                </div>
            </div>
            </div>
        </div>
    )
}

export default CreateSong;
