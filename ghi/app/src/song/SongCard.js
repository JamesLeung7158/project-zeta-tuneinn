const SongCard = ({ songs }) => {

    return (
        <div className="col">
            {songs?.map((song) => {
                return (
                    <div className="col-md-4 mb-4" key={song.id}>
                    <div className="card shadow h-100 w-50">
                    <div className="card-header">
                        <h5 className="card-title fs-6">Song:</h5>
                        <p className="card-text fs-4">{song.title}</p>
                        <h5 className="card title fs-6">Artist:</h5>
                        <p className="card-text fs-4">{song.artist}</p>
                    </div>
                    </div>
                    </div>
                )
            })}
        </div>
    )
}

export default SongCard;
