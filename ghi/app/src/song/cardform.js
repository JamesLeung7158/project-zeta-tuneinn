import React, { useState } from 'react';

function CreateCard() {

    const [user_name, setUsername] = useState('');
    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('');
    const [date, setDate] = useState('');
    const [picture_url, setPictureUrl] = useState('');

    const handleUsername = (event) => {
        const value = event.target.value;
        setUsername(value)
    }

    const handleFirstname = (event) => {
        const value = event.target.value;
        setFirstName(value)
    }

    const handleLastname = (event) => {
        const value = event.target.value;
        setLastName(value)
    }

    const handleDate = (event) => {
        const value = event.target.value;
        setDate(value)
    }

    const handlePictureUrl = (event) => {
        const value = event.target.value;
        setPictureUrl(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.user_name = user_name;
        data.first_name = first_name;
        data.last_name = last_name;
        data.date = date;
        data.picture_url = picture_url;

        const newCardUrl = ""
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            header: {
                "Content-Type": "application/json",
            }
        }

        // Need to add the url path for newCardUrl
        const response = await fetch(newCardUrl, fetchConfig);
        if (response.ok) {
            setUsername('');
            setFirstName('');
            setLastName('');
            setDate('');
            setPictureUrl('');
        }
    }

    return (
        <>
            <div className="row">
                <div className="col">
                    <h1>Create a Card</h1>
                    <form onSubmit={handleSubmit} id="create-card">
                        <div>
                            <label htmlFor="username">User Name</label>
                            <input
                                value={user_name}
                                onChange={handleUsername}
                                required
                                type="text"
                                placeholder="Username"
                                id="username"
                                >
                            </input>
                        </div>
                        <div>
                            <label htmlFor="first_name">First Name</label>
                            <input
                                value={first_name}
                                onChange={handleFirstname}
                                required
                                type="text"
                                placeholder="First Name"
                                id="firstname"
                                >
                            </input>
                        </div>
                        <div>
                            <label htmlFor="lastname">Last Name</label>
                            <input
                                value={last_name}
                                onChange={handleLastname}
                                required
                                type="text"
                                placeholder="Last name"
                                id="last_name"
                                >
                            </input>
                        </div>
                        <div>
                            <label htmlFor="date">Date</label>
                            <input
                                value={date}
                                onChange={handleDate}
                                required
                                type="date"
                                placeholder="Date"
                                id="date"
                                >
                            </input>
                        </div>
                        <div>
                            <label htmlFor="picture_url">Picture Url</label>
                            <input
                                value={picture_url}
                                onChange={handlePictureUrl}
                                required
                                type="text"
                                placeholder="Picture Url"
                                id="picture_url"
                                >
                            </input>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </>
    )
}

export default CreateCard;
