import React, { useState } from 'react';

function CreateAccount() {

    const [username, setUsername] = useState('');
    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [horoscope, setHoroscope] = useState('');

    const handleUsername = (event) => {
        const value = event.target.value;
        setUsername(value)
    }

    const handleFirstName = (event) => {
        const value = event.target.value;
        setFirstName(value)
    }

    const handleLastName = (event) => {
        const value = event.target.value;
        setLastName(value)
    }

    const handleEmail = (event) => {
        const value = event.target.value;
        setEmail(value)
    }

    const handleHoroscope = (event) => {
        const value = event.target.value;
        setHoroscope(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.username = username;
        data.first_name = first_name;
        data.last_name = last_name;
        data.email = email;
        data.horoscope = horoscope;

        const newAccountUrl = 'http://localhost:8090/users/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(newAccountUrl, fetchConfig)
        if (response.ok) {
            setUsername('');
            setFirstName('');
            setLastName('');
            setEmail('');
            setHoroscope('');
        }
    }

    return (
        <>
            <div className="row">
                <div className="col">
                    <h1>Create Account</h1>
                    <form onSubmit={handleSubmit} id="create-account">
                        <div>
                            <label htmlFor="username">Username</label>
                            <input
                            value={username}
                            onChange={handleUsername}
                            required
                            type="text"
                            placeholder="Username"
                            id="username"
                            />
                        </div>
                        <div>
                            <label htmlFor="first_name">First Name</label>
                            <input
                            value={first_name}
                            onChange={handleFirstName}
                            required
                            type="text"
                            placeholder="First Name"
                            id="first_name"
                            />
                        </div>
                        <div>
                            <label htmlFor="last_name">Last Name</label>
                            <input
                            value={last_name}
                            onChange={handleLastName}
                            required
                            type="text"
                            placeholder="Last Name"
                            id="last_name"
                            />
                        </div>
                        <div>
                            <label htmlFor="email">Email</label>
                            <input
                            value={email}
                            onChange={handleEmail}
                            required
                            type="text"
                            placeholder="Email"
                            id="email"
                            />
                        </div>
                        <div>
                            <label htmlFor="horoscope">Horoscope</label>
                            <input
                            value={horoscope}
                            onChange={handleHoroscope}
                            required
                            type="text"
                            placeholder="Horoscope"
                            id="horoscope"
                            />
                        </div>
                    </form>
                </div>
            </div>
        </>
    )
}

export default CreateAccount;
