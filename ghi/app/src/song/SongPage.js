import React, { useState, useEffect } from 'react';
import CreateSong from './CreateSong';
import SongCard from './SongCard';

function SongList() {

    const [songs, setSongs] = useState([]);

    async function fetchSongData() {
        const songsUrl = 'http://localhost:8090/cards/songs/';
        const response = await fetch(songsUrl);
        console.log(response)
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setSongs(data.songs);
            console.log(data.songs)
        }
    }

    // const resetForm = () => {
    //     const form = document.getElementById('songForm');
    //     form.reset();
    // }

    useEffect(() => {
        fetchSongData()
    }, []);

    return (
        <>
        <br />
        <h1 className="text-center"><strong>Song List</strong></h1>
        <br />
        <div className="container mt-4">
            <div className="row justify-content-end text-center">
                <button
                type="button"
                className="btn btn-primary btn-lg btn-block"
                data-bs-toggle="modal"
                data-bs-target="#createsong"
                data-bs-whatever="@mdo"
                >
                Create Song
                </button>
            </div>
        </div>
        <CreateSong />

        <div className="container mt-4">
        <div className="row gy-3">
            <SongCard songs={songs} />
        </div>
        </div>
        </>
    )
}

export default SongList;
