from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse

class User(AbstractUser):
    username = models.CharField(max_length=200, unique=True)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    email = models.EmailField(unique=True)
    horoscope = models.CharField(max_length=200, null=True)
    
    def get_api_url(self):
        return reverse("show_user", kwargs={"id": self.id})