from django.urls import path

from .views import list_users, show_user

urlpatterns = [
    path("users/", list_users, name="list_users"),
    path(
        "users/<int:id>/",
        show_user,
        name= "show_user",
    ),
]
