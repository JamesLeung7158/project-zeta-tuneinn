from django.shortcuts import render
from .models import User
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from django.http import JsonResponse

class UserEncoder(ModelEncoder):
    model = User
    properties = ["id", "username", "email", "first_name", "last_name", "horoscope"]


@require_http_methods(["GET", "POST"])
def list_users(request):
    if request.method == "GET":
        users = User.objects.exclude(email="")
        return JsonResponse(
            {"users": users},
            encoder=UserEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
    try:
        user = User.objects.create_user(
            username = content["username"],
            first_name = content["first_name"],
            last_name = content["last_name"],
            email = content["email"],
            password = content["password"],
            horoscope = content["horoscope"],
        )
        return JsonResponse(
            user,
            UserEncoder,
            safe=False
        )
    except ValueError:
        return JsonResponse(
                {"message": "Cannot create user"},
                status=400,
            )

@require_http_methods(["GET", "PUT", "DELETE"])
def show_user(request, id):
    if request.method == "GET":
        user = User.objects.get(id = id)
        return JsonResponse(
            user,
            encoder = UserEncoder,
            safe=False
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        User.objects.filter(id=id).update(**content)
        user = User.objects.get(id=id)
        return JsonResponse(
            user,
            encoder=UserEncoder,
            safe=False
        )
    else:
        count, _ = User.objects.get(id=id).delete()

        return JsonResponse(
                {"User Deleted": count > 0}
            )
